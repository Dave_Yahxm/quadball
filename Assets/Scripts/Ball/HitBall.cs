﻿using UnityEngine;

public class HitBall : MonoBehaviour {
    private int damage;
    private Utilities.BallType myBallType;
    public void SetBallType(Utilities.BallType ballType) {
        myBallType = ballType;
        damage = (int)ballType;
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag == "Enemy") {
            other.gameObject.GetComponent<IDamageable<int>>().Hit(damage, myBallType);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "PowerUp") {
            other.GetComponent<PowerUp>().Picked();
            SoundManager.instance.Play("Hit");
        }
    }
}
