﻿using UnityEngine;

public class Ball : MonoBehaviour, IPooledObject {
    private Rigidbody2D rb;
    [SerializeField] private float thrust = 5f;

    private void Awake() {
        rb = gameObject.GetComponent<Rigidbody2D>();
        DontDestroyOnLoad(gameObject);
    }

    private void OnEnable() {
        Events.restartGameEvent += delegate { gameObject.SetActive(false); };
    }

    private void OnDisable() {
        Events.restartGameEvent -= delegate { gameObject.SetActive(false); };
    }

    private void OnBecameInvisible() {
        gameObject.SetActive(false);
    }

    public void ResetObject() {
        rb.isKinematic = false;
        rb.AddForce(transform.up * thrust, ForceMode2D.Impulse);
    }
}
