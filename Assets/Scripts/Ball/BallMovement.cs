﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BallMovement : MonoBehaviour {
    [SerializeField] private float speed = 10.0f;
    private Rigidbody2D rb;
    private Vector2 targetPosition;

    private void OnValidate() {
        this.enabled = true;
    }

    private void Awake() {
        rb = gameObject.GetComponent<Rigidbody2D>();
    }
    private void Start() {
        this.enabled = false;
    }

    private void Update() {
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        if (Vector2.Distance(transform.position, targetPosition) <= 0) {
            this.enabled = false;
            gameObject.SetActive(false);
        }
    }

    public void Move() {
        rb.isKinematic = true;
        rb.velocity = Vector2.zero;
        rb.angularVelocity = 0f;
        targetPosition = BallManager.instance.firstBallPosition;
        transform.position = new Vector2(transform.position.x, targetPosition.y);
        this.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Destroyer") {
            if (BallManager.instance.firstBallPosition == Vector2.zero)
                BallManager.instance.firstBallPosition = transform.position;
            Move();
        }
    }
}