﻿public static class Events {

    public delegate void GameOverEvent();
    public static event GameOverEvent gameOverEvent;

    public delegate void EndLaunchEvent();
    public static event EndLaunchEvent endLaunchEvent;

    public delegate void RestartGameEvent();
    public static event RestartGameEvent restartGameEvent;
    public static void RestartGameInvoke() {
        restartGameEvent?.Invoke();
    }

    public static void GameOverInvoke() {
        gameOverEvent?.Invoke();
    }

    public static void EndLaunchInvoke() {
        endLaunchEvent?.Invoke();
    }
}
