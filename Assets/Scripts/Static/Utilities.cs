﻿using System.Collections.Generic;
using UnityEngine;

public static class Utilities {
    public enum BallType { normal = 1, fire = 5, explosive = 20 };
    public enum TagsEnum {
        Ball,
        Square,
        Circle,
        Plus,
        Fire,
        Explosive
    }


    public static int[] GenerateRandom(int count, int min, int max) {
        int[] generatedList = new int[count];//new array

        for (int i = 0; i < count; i++)//Start array creation
        {
            //Generate random number
            int j = Random.Range(0, i + 1);

            generatedList[i] = generatedList[j];
            generatedList[j] = min + i;
        }
        return generatedList;
    }

    public static void Spawn(this Transform trans, Transform spawnPoint) {
        trans.position = spawnPoint.position;
        trans.rotation = spawnPoint.rotation;
        trans.gameObject.SetActive(true);
    }

    public static void ResetObject(GameObject[] pool) {
        foreach (GameObject item in pool) {
            if (item.activeSelf) item.GetComponent<Element>().DestroyElement();
        }
    }

    public static void ResetObject(List<GameObject> pool) {
        foreach (GameObject item in pool) {
            item.SetActive(false);
        }
    }
}
