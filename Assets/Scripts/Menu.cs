﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {
    public void Play() {
        SceneManager.LoadScene("Game");
    }

    public void Quit() {
        //Application.Quit();
        Events.GameOverInvoke();
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
        Time.timeScale = 1;

    }
}
