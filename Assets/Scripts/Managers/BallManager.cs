﻿using System.Collections;
using UnityEngine;

public class BallManager : MonoBehaviour {
    public static BallManager instance;
    public int nBalls = 1;
    [SerializeField] private float delayShoot = 0.5f;
    private Transform ArrowTransform;
    public Vector2 firstBallPosition;
    public Utilities.BallType powerUp;


    private void Awake() {
        instance = this;
        powerUp = Utilities.BallType.normal;
        ArrowTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    public void ShootBalls() {
        StartCoroutine(WaitAndShoot(nBalls));
    }

    private IEnumerator WaitAndShoot(int nBalls) {
        for (int i = 0; i < nBalls - 1; i++) {
            ObjectPooler.instance.GetObjectFromPool(Utilities.TagsEnum.Ball, ArrowTransform, Utilities.BallType.normal);
            yield return new WaitForSeconds(delayShoot);
        }
        ObjectPooler.instance.GetObjectFromPool(Utilities.TagsEnum.Ball, ArrowTransform, powerUp);
        powerUp = Utilities.BallType.normal;
        this.enabled = true;
    }

    private void LateUpdate() {
        if (!GameObject.FindWithTag("Ball")) {
            this.enabled = false;
            EnemyManager.instance.IncreasesEnemyHealth();
            GameManager.instance.IncreaseScore();
            Events.EndLaunchInvoke();
            ArrowTransform.GetComponent<DragAndDrop>().enabled = true;
            ArrowTransform.GetComponent<ArrowMovement>().Move();
            firstBallPosition = Vector2.zero;
        }
    }
}
