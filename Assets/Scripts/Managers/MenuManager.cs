using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    [SerializeField] private GameObject gameOverMenu;
    [SerializeField] private GameObject newHighScoreMenu;
    [SerializeField] private GameObject highScoresTable;
    [SerializeField] private GameObject HighScoreManager;
    private HighScoreManager highScoreManager;

    private void Start() {
        highScoreManager = HighScoreManager.GetComponent<HighScoreManager>();
    }

    public void PauseGame() {
        Time.timeScale = 0;
    }

    public void ResumeGame() {
        Time.timeScale = 1;
    }

    public void RestartGame() {
        BallManager.instance.enabled = false;
        Events.RestartGameInvoke();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        ResumeGame();
    }

    public void ShowHighScoreTable() {
        gameOverMenu.SetActive(false);
        highScoreManager.ShowHighScoreTable();
        highScoresTable.SetActive(true);
    }
}