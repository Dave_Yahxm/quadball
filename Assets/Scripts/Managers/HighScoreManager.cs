﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreManager : MonoBehaviour {
    private Transform entryTemplate;
    private Transform highScoreContainer;
    [SerializeField] private Transform highScoreTable;
    [SerializeField] private int nScore = 3;

    private List<Transform> highScoreEntryTransformList;
    private HighScores highScores;

    private void Start() {
        highScoreContainer = highScoreTable.Find("HighScoresContainer");
        entryTemplate = highScoreContainer.Find("HighScoreTemplate");
    }
    private void Awake() {
        highScores = new HighScores();
        highScores.highScoreEntryList = new List<HighScoreEntry>();
        highScoreEntryTransformList = new List<Transform>();
        LoadHighScores();
    }

    public int GetHighScore() {
        if (highScores.highScoreEntryList.Count != 0)
            return highScores.highScoreEntryList[0].score;
        else
            return 0;
    }

    private void LoadHighScores() {
        string jsonString = PlayerPrefs.GetString("highScoreTable");
        if (jsonString.Length != 0) {
            highScores = JsonUtility.FromJson<HighScores>(jsonString);
        }
    }

    private void SaveHighScores() {
        string json = JsonUtility.ToJson(highScores);
        PlayerPrefs.SetString("highScoreTable", json);
        PlayerPrefs.Save();
    }

    public void AddHighScoreEntry(string name, int score) {
        HighScoreEntry highScoreEntry = new HighScoreEntry { name = name, score = score };

        LoadHighScores();

        if (highScores.highScoreEntryList.Count >= nScore) {
            highScores.highScoreEntryList.RemoveAt(nScore - 1);
        }
        highScores.highScoreEntryList.Add(highScoreEntry);

        for (int i = 0; i < highScores.highScoreEntryList.Count; i++) {
            for (int j = i + 1; j < highScores.highScoreEntryList.Count; j++) {
                if (highScores.highScoreEntryList[j].score > highScores.highScoreEntryList[i].score) {
                    HighScoreEntry tmp = highScores.highScoreEntryList[i];
                    highScores.highScoreEntryList[i] = highScores.highScoreEntryList[j];
                    highScores.highScoreEntryList[j] = tmp;
                }
            }
        }
        SaveHighScores();
    }
    private void CreateHighScoreEntryTransform(HighScoreEntry highScoreEntry, Transform container, List<Transform> transformList) {
        float templateHeight = 100f;
        int transformListCount = transformList.Count;
        Transform entryTransform = Instantiate(entryTemplate, container);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, (-templateHeight * transformListCount) - 50f);
        entryTransform.gameObject.SetActive(true);

        int rank = transformListCount + 1;
        entryTransform.Find("PosText").GetComponent<Text>().text = rank.ToString();

        int score = highScoreEntry.score;
        entryTransform.Find("ScoreText").GetComponent<Text>().text = score.ToString();

        string name = highScoreEntry.name;
        entryTransform.Find("NameText").GetComponent<Text>().text = name;

        transformList.Add(entryTransform);
    }
    public void ShowHighScoreTable() {
        LoadHighScores();
        highScoreEntryTransformList.Clear();
        foreach (HighScoreEntry highScoreEntry in highScores.highScoreEntryList) {
            CreateHighScoreEntryTransform(highScoreEntry, highScoreContainer, highScoreEntryTransformList);
        }
    }

    private class HighScores {
        public List<HighScoreEntry> highScoreEntryList;
    }

    [System.Serializable]
    private class HighScoreEntry {
        public string name;
        public int score;
    }
}
