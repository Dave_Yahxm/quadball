﻿using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    [SerializeField] private Text ScoreText;
    [SerializeField] private Text HighScoreText;
    [SerializeField] private GameObject HighScoreManager, GameOverPanel;
    [SerializeField] private GameObject NewHighScorePanel;
    private GameObject Arrow;
    private HighScoreManager highScoreManager;
    private InputField inputField;

    private int score = 0;

    private void Awake() {
        instance = this;
    }

    private void Start() {
        Arrow = GameObject.FindGameObjectWithTag("Player");
        inputField = NewHighScorePanel.transform.GetComponentInChildren<InputField>();
        highScoreManager = HighScoreManager.GetComponent<HighScoreManager>();
        HighScoreText.text = "HighScore: " + highScoreManager.GetHighScore().ToString();
    }
    public void IncreaseScore() {
        score++;
        ScoreText.text = "Score: " + score.ToString();
    }

    public void GameOver() {
        Events.GameOverInvoke();
        CheckHighScore();
        Arrow.GetComponent<DragAndDrop>().enabled = false;
    }

    private void CheckHighScore() {
        int highScore = highScoreManager.GetHighScore();
        if (score > highScore) {
            NewHighScorePanel.SetActive(true);
        } else {
            GameOverPanel.SetActive(true);
        }
    }

    public void AddHighScoreEntry() {
        string name = inputField.transform.Find("Text").GetComponent<Text>().text;
        if (name.Length == 0) return;
        highScoreManager.AddHighScoreEntry(name, score);
        NewHighScorePanel.SetActive(false);
        GameOverPanel.SetActive(true);
    }
}
