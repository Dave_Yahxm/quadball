﻿using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour {
    public static SoundManager instance;

    public AudioMixerGroup mixerGroup;

    public Sound[] sounds;

    void Start() {
        if (instance != null) {
            Destroy(gameObject);
        } else {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        foreach (Sound s in sounds) {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.loop = s.loop;

            s.source.outputAudioMixerGroup = mixerGroup;
        }

        Play("MainTheme");
    }

    public void Play(string soundName) {
        if (soundName.Length == 0) {
            Debug.Log("Inserire parametro al play()!");
        }
        foreach (Sound s in sounds) {
            if (s.name == soundName) {
                s.source.Play();
                return;
            }
        }
        Debug.Log("Sound non trovato!");
    }


    [System.Serializable]
    public class Sound {

        public string name;

        public AudioClip clip;

        [Range(0f, 1f)]
        public float volume;

        [Range(-2f, 2f)]
        public float pitch;

        public bool loop = false;

        [HideInInspector]
        public AudioSource source;
    }
}
