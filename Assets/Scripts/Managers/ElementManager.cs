﻿using UnityEngine;

public class ElementManager : MonoBehaviour {

    Transform SpawnPointGroup;
    private int nSpawnPoint;
    private bool gameOver = false;

    private void OnEnable() {
        Events.endLaunchEvent += SpawnElement;
    }

    private void OnDisable() {
        Events.endLaunchEvent -= SpawnElement;
    }

    private void Start() {
        SpawnPointGroup = GameObject.FindWithTag("Respawn").transform;
        nSpawnPoint = SpawnPointGroup.childCount;
        SpawnElement();
    }
    private void SpawnElement() {
        Transform newTransform;
        int[] idxPosition = Utilities.GenerateRandom(nSpawnPoint, 0, nSpawnPoint - 1);
        int nEnemy = Random.Range(1, nSpawnPoint - 1);
        //Spawn Enemy
        for (int i = 0; i < nEnemy; i++) {
            newTransform = SpawnPointGroup.GetChild(idxPosition[i]).transform;
            switch (Random.Range(0, 2)) {
                case 0:
                    ObjectPooler.instance.GetObjectFromPool(Utilities.TagsEnum.Square, newTransform);
                    break;
                case 1:
                    ObjectPooler.instance.GetObjectFromPool(Utilities.TagsEnum.Circle, newTransform);
                    break;
            }
        }
        //Spawn bonus
        if ((nSpawnPoint - nEnemy) >= 2) {
            newTransform = SpawnPointGroup.GetChild(idxPosition[nEnemy]).transform;
            switch (Random.Range(0, 2)) {
                case 0:
                    ObjectPooler.instance.GetObjectFromPool(Utilities.TagsEnum.Fire, newTransform);
                    break;
                case 1:
                    ObjectPooler.instance.GetObjectFromPool(Utilities.TagsEnum.Explosive, newTransform);
                    break;
            }
        }
        //Spawn plus
        newTransform = SpawnPointGroup.GetChild(idxPosition[nSpawnPoint - 1]).transform;
        ObjectPooler.instance.GetObjectFromPool(Utilities.TagsEnum.Plus, newTransform);

    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Enemy" && !gameOver) {
            GameManager.instance.GameOver();
            gameOver = true;
        }
    }
}
