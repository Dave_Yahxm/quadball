﻿using UnityEngine;

public class EnemyManager : MonoBehaviour {
    public static EnemyManager instance;
    public int healthEnemy = 1;

    private void Awake() {
        instance = this;
    }

    public void IncreasesEnemyHealth() {
        switch (healthEnemy) {
            case 5:
                healthEnemy += 5;
                break;

            case 15:
                healthEnemy += 10;
                break;

            case 30:
                healthEnemy += 20;
                break;

            default:
                healthEnemy += 1;
                break;
        }
    }
}
