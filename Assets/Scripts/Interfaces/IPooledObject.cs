﻿using UnityEngine;
public interface IPooledObject {
    void ResetObject();
}
