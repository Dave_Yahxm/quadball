﻿public interface IDamageable<T> {
    void Hit(T damageTake, Utilities.BallType ballType);
}
