using UnityEngine;

public class Element : MonoBehaviour, IPooledObject {
    private Rigidbody2D rb;

    protected virtual void Awake() {
        rb = GetComponent<Rigidbody2D>();
        DontDestroyOnLoad(gameObject);
    }

    private void OnEnable() {
        Events.gameOverEvent += GameOver;
        Events.restartGameEvent += DestroyElement;
    }

    private void OnDisable() {
        Events.gameOverEvent -= GameOver;
        Events.restartGameEvent -= DestroyElement;
    }

    private void OnBecameInvisible() {
        DestroyElement();
    }

    public virtual void DestroyElement() {
        gameObject.SetActive(false);
    }

    protected virtual void GameOver() {
        if (gameObject.activeInHierarchy) {
            GetComponent<ElementMovement>().enabled = false;
            rb.isKinematic = false;
            rb.AddForce(Random.insideUnitCircle * 5f, ForceMode2D.Impulse);
        }
    }

    public virtual void ResetObject() {
        transform.localScale = Vector2.zero;
        rb.isKinematic = true;
    }
}
