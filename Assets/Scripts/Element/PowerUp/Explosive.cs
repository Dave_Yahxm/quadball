public class Explosive : PowerUp {
    public override void Picked() {
        BallManager.instance.powerUp = Utilities.BallType.explosive;
        base.Picked();
    }
}