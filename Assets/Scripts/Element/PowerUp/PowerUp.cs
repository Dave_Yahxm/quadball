﻿using UnityEngine;

public class PowerUp : Element {
    public Utilities.BallType powerUpType;
    [SerializeField] private CircleCollider2D circleColl;

    protected override void Awake() {
        base.Awake();
        circleColl = GetComponent<CircleCollider2D>();
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Finish") {
            DestroyElement();
        }
    }
    public virtual void Picked() {
        DestroyElement();
    }

    protected override void GameOver() {
        base.GameOver();
        circleColl.enabled = false;
    }

    public override void ResetObject() {
        base.ResetObject();
        circleColl.enabled = true;
    }
}
