﻿using UnityEngine;

public class Square : Enemy {

    [SerializeField] private BoxCollider2D boxColl;

    protected override void Awake() {
        base.Awake();
        boxColl = GetComponent<BoxCollider2D>();
    }
    protected override void GameOver() {
        base.GameOver();
        boxColl.enabled = false;
    }

    public override void ResetObject() {
        boxColl.enabled = true;
        base.ResetObject();
    }
}
