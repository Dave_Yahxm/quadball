﻿using System.Collections;
using UnityEngine;

public class Enemy : Element, IDamageable<int> {
    private float maxHealth, health;
    private TextMesh healthText;
    private Material materialEnemy;

    private void OnValidate() {
        this.enabled = true;
    }

    protected override void Awake() {
        base.Awake();
        materialEnemy = gameObject.GetComponent<Renderer>().material;
        healthText = gameObject.GetComponentInChildren<TextMesh>();
    }

    public override void DestroyElement() {
        base.DestroyElement();
        StopAllCoroutines();
    }

    public override void ResetObject() {
        base.ResetObject();
        materialEnemy.color = Color.green;
        healthText.color = Color.green;
        maxHealth = EnemyManager.instance.healthEnemy;
        health = maxHealth;
        healthText.text = health.ToString();
    }

    private void ChangeColor() {
        Color healthColor = Color.Lerp(Color.red, Color.green, (health / maxHealth));
        materialEnemy.color = healthColor;
        healthText.color = healthColor;
    }

    public void Hit(int damageTake) {
        health -= damageTake;
        healthText.text = health.ToString();
        ChangeColor();
        if (health <= 0) {
            DestroyElement();
            SoundManager.instance.Play("Destroy");
        }
    }

    public void Hit(int damageTake, Utilities.BallType ballType) {
        switch (ballType) {
            case Utilities.BallType.explosive:
                Hit(damageTake);
                //Attivare effetto Particellare Esplosione
                break;
            case Utilities.BallType.fire:
                StartCoroutine(Burn(damageTake));
                //ATTIVARE EFFETTO PARTICELLARE FUOCO
                break;
            default:
                Hit(damageTake);
                break;
        }
    }

    private IEnumerator Burn(int damage) {
        for (int i = 0; i < damage; i++) {
            Hit(1);
            yield return new WaitForSeconds(0.5f);
        }
    }
}
