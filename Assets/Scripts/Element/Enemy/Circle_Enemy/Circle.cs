﻿using UnityEngine;

public class Circle : Enemy {

    [SerializeField] private CircleCollider2D circleColl;

    protected override void Awake() {
        base.Awake();
        circleColl = GetComponent<CircleCollider2D>();
    }

    protected override void GameOver() {
        base.GameOver();
        circleColl.enabled = false;
    }

    public override void ResetObject() {
        base.ResetObject();
        circleColl.enabled = true;
    }
}
