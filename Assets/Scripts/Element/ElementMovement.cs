﻿using UnityEngine;

public class ElementMovement : MonoBehaviour {
    private Vector2 targetPosition;
    [SerializeField] private float speed = 7f;


    /*public void OnEnable() {
        targetPosition = transform.position;
    }*/

    private void LateUpdate() {
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        if (Vector2.Distance(targetPosition, transform.position) <= 0) this.enabled = false;
    }
    public void Move() {
        if (gameObject.activeSelf) {
            targetPosition.y -= 0.75f;
            this.enabled = true;
        }
    }

    private void OnBecameVisible() {
        targetPosition = transform.position;
        Events.endLaunchEvent += Move;
    }

    private void OnBecameInvisible() {
        Events.endLaunchEvent -= Move;
    }
}
