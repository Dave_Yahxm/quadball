﻿using UnityEngine;

public class DragAndDrop : MonoBehaviour {

    private Touch touch;

    private Vector2 startPosition, trackerPosition, direction;
    private float LenghtDashedLine = 0.5f;


    public GameObject Tracker, DashedLine;

    //Rotazione freccia.
    private float angleRotation = 0;
    private float smoothRotation = 20f;

    void Update() {
        if (Input.touchCount > 0) {
            touch = Input.GetTouch(0);
            switch (touch.phase) {
                case TouchPhase.Began:
                    startPosition = Camera.main.ScreenToWorldPoint(touch.position);
                    break;

                case TouchPhase.Moved:
                    if (!Tracker.activeSelf) {
                        Tracker.transform.position = startPosition;
                        Tracker.SetActive(true);
                        DashedLine.SetActive(true);
                    }
                    trackerPosition = Camera.main.ScreenToWorldPoint(touch.position);
                    direction = startPosition - trackerPosition;
                    angleRotation = Mathf.Atan2(-direction.x, direction.y) * Mathf.Rad2Deg; // Angolo di rotazione della freccia.
                    if (direction.y > 0 & angleRotation < 80 & angleRotation > -80) { //Limito i movimenti della freccia.
                        Quaternion target = Quaternion.Euler(0, 0, angleRotation); //Definisco un target, cioè un angolo (angleRotation) che la freccia deve raggiungere.
                        gameObject.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, target, Time.deltaTime * smoothRotation);
                        DashedLine.transform.rotation = Quaternion.Slerp(DashedLine.transform.rotation, target, Time.deltaTime * smoothRotation); //Raggiunge il target definito precedentemente
                        DashedLine.transform.localScale = new Vector3(direction.y * 2, direction.y * 2, 1f); //La sua lunghezza dipende da quanta differenza c'è tra la posizione iniziale e finale del tocco
                    }
                    break;

                case TouchPhase.Ended:
                    if (DashedLine.transform.localScale.y > LenghtDashedLine & DashedLine.activeSelf & direction.y > 0 & angleRotation < 80 & angleRotation > -80) {
                        BallManager.instance.ShootBalls();
                        this.enabled = false;
                    }
                    Tracker.SetActive(false); //Disattivo il tracker.
                    DashedLine.SetActive(false); //Disattivo la linea traiettoria.
                    break;
            }
        }

        if (Input.GetKeyDown("space")) {
            BallManager.instance.ShootBalls();
            this.enabled = false;
        }
    }
}
