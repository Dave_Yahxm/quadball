﻿using UnityEngine;

public class ArrowMovement : MonoBehaviour {

    [SerializeField] private float speed = 10.0f;
    private Vector2 targetPosition;


    private void OnValidate() {
        this.enabled = false;
    }
    private void Update() {
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        if (Vector2.Distance(transform.position, targetPosition) <= 0) this.enabled = false;
    }

    public void Move() {
        targetPosition = new Vector2(BallManager.instance.firstBallPosition.x, transform.position.y);
        this.enabled = true;
    }
}
