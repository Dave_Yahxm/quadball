﻿using UnityEngine;

public class CameraAspect : MonoBehaviour {
    public float width = 9f;
    public float height = 16f;

    void Awake() {
        Camera.main.aspect = width / height;
    }
}