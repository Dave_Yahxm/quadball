﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour {
    [System.Serializable]
    public class Pool {
        public Utilities.TagsEnum tag;
        public GameObject prefab;
        public int size;
    }

    public static ObjectPooler instance;

    public List<Pool> pools;
    public Dictionary<Utilities.TagsEnum, Queue<GameObject>> poolDictionary;

    private void Awake() {
        if (instance) {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start() {
        poolDictionary = new Dictionary<Utilities.TagsEnum, Queue<GameObject>>();

        foreach (Pool pool in pools) {
            Queue<GameObject> objectPool = new Queue<GameObject>();
            for (int i = 0; i < pool.size; i++) {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }

            poolDictionary.Add(pool.tag, objectPool);
        }
    }

    public GameObject GetObjectFromPool(Utilities.TagsEnum tag, Transform spawnTransform) {
        if (!poolDictionary.ContainsKey(tag)) {
            Debug.Log("Pool with tag " + tag + "doesn't exist");
            return null;
        }
        GameObject objectToSpawn = poolDictionary[tag].Dequeue();

        objectToSpawn.transform.Spawn(spawnTransform);
        objectToSpawn.GetComponent<IPooledObject>().ResetObject();

        poolDictionary[tag].Enqueue(objectToSpawn);
        return objectToSpawn;
    }

    public GameObject GetObjectFromPool(Utilities.TagsEnum tag, Transform spawnTransform, Utilities.BallType ballType) {
        if (!poolDictionary.ContainsKey(tag)) {
            Debug.Log("Pool with tag " + tag + "doesn't exist");
            return null;
        }
        GameObject objectToSpawn = poolDictionary[tag].Dequeue();

        objectToSpawn.transform.Spawn(spawnTransform);
        objectToSpawn.GetComponent<HitBall>().SetBallType(ballType);
        objectToSpawn.GetComponent<IPooledObject>().ResetObject();

        poolDictionary[tag].Enqueue(objectToSpawn);
        return objectToSpawn;
    }
}
